## Contribution guide

If you're reading this, you're probably going to contribute something to the overlay, aren't you? (-:
This page is also a guide for the overlay developers and maintainers.

If you really want to contribute something, please keep in mind these rules:

+ Don't contribute the ebuilds. Ebuilds we can't use will not be maintained, 
and the ebuilds we can't maintain are removed from the overlay.
Instead, put the ebuild into your own overlay so you can maintain it.
+ If you want to maintain something in our overlay, then contact the developers by creating an issue.

If you're already a maintainer or a developer:

+ Don't commit the ebuilds you didn't test properly! This may seem obvious, but it is worth mentioning.
+ Use your common sense while naming and categorizing the ebuilds. For example, a video player
that can play audio files is surely put into the `media-video` category.
+ The `-9999` ebuilds have to be checked once a month. Any upstream could break something
one day, and your ebuild's one is not an exception.
+ Every ebuild that you or somebody else don't maintain has to be masked for removal.
Gentoo upstream masks for removal in 30 days and we use the same timing.
+ A cleanup has to be done at least once a few months.
+ The other maintainer's ebuilds haven't to be changed without his or her permission.
+ Every single file change should be commited with a single commit. The manifest file should be commited after the ebuild it belongs to. 
+ Patches which are shared among the ebuilds should be commited with the first ebuild they are used in. A single patch for one ebuild has to be commited with the ebuild.
+ The `package.mask` file has to be commited after every other file, just before you push.

That is all for now.
